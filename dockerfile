# Pull nodejs15.0 image
FROM node:15.0.1-alpine3.10

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install the nestjs CLI globally
RUN npm i -g @nestjs/cli

# Bundle app source
COPY package.json .
RUN npm install
COPY . .