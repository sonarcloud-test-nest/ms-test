import { Injectable, NotFoundException, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, IsNull, Not } from 'typeorm';
import { Playlist } from '../entity/playlist.entity';
import { PlaylistDto } from './dto/playlist.dto';

@Injectable()
export class PlaylistService {
    constructor(
        @InjectRepository(Playlist)
        private readonly playlistRepository: Repository<Playlist>
    ) {}

    all(): Promise<Playlist[]> {
        return this.playlistRepository.find();
    }

    find(id: string): Promise<Playlist> {
        return this.playlistRepository.findOneOrFail(id);
    }

    findByUser(id: string): Promise<Playlist[]> {
        return this.playlistRepository.find({
            relations: ['collection', 'book'],
            where: {
                userId: id
            }
        });
    }

    findCollectionsByUser(id: string): Promise<Playlist[]> {
        return this.playlistRepository.find({
            relations: ['collection'],
            where: {
                userId: id,
                collectionId: Not(IsNull())
            },
            order: {
                createdAt: "DESC"
            }
        });
    }

    findBooksByUser(id: string): Promise<Playlist[]> {
        return this.playlistRepository.find({
            relations: ['book'],
            where: {
                userId: id,
                bookId: Not(IsNull())
            }
        });
    }

    findByBookAndUser(bookId, userId): Promise<Playlist> {
        return this.playlistRepository.findOne({
            where: {
                userId: userId,
                bookId: bookId
            }
        })
    }

    async create(playlist: PlaylistDto): Promise<Playlist> {
        let list = await this.playlistRepository.findOne({
            where: [
                {userId: playlist.userId, bookId: playlist.bookId, collectionId: null},
                {userId: playlist.userId, collectionId: playlist.collectionId, bookId: null},
            ]
        });

        if (list) {
            await console.log(playlist, list)
            throw new HttpException('The playlist already exist', 400);
        }

        return await this.playlistRepository.save(playlist);
    }

    async remove(id: string): Promise<void> {
        let playlist = await this.playlistRepository.findOne(id);

        if (!playlist) {
            throw new NotFoundException(`This playlist '${id}' doesn't exist.`)
        }

        await this.playlistRepository.remove(playlist);
    }
}