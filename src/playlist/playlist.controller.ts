import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PlaylistService } from './playlist.service';
import { Playlist } from '../entity/playlist.entity';
import { PlaylistDto } from './dto/playlist.dto';

@ApiTags('Playlist')
@Controller('playlist')
export class PlaylistController {

    constructor(private playlistService: PlaylistService) {}

    @Get()
    async all(): Promise<Playlist[]> {
        return await this.playlistService.all();
    }

    @Get(':id')
    async find(@Param('id') id: string): Promise<Playlist> {
        return await this.playlistService.find(id);
    }

    @Get('user/:userId/book/:bookId')
    async findByBookAndUser(@Param('userId') userId: string, @Param('bookId') bookId: string): Promise<Playlist> {
        return await this.playlistService.findByBookAndUser(bookId, userId);
    }

    @Get('user/:userId')
    async findByUser(@Param('userId') userId: string): Promise<Playlist[]> {
        return await this.playlistService.findByUser(userId);
    }

    @Get('user/:userId/collections')
    async findCollectionsByUser(@Param('userId') userId: string): Promise<Playlist[]> {
        return await this.playlistService.findCollectionsByUser(userId);
    }

    @Get('user/:userId/books')
    async findBooksByUser(@Param('userId') userId: string): Promise<Playlist[]> {
        return await this.playlistService.findBooksByUser(userId);
    }

    @Post('add')
    async create(@Body() playlist: PlaylistDto) {
        return await this.playlistService.create(playlist);
    }

    @Delete('delete/:id')
    async delete(@Param('id') id:string): Promise<void> {
        return await this.playlistService.remove(id);
    }
}