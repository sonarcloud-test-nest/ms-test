import { ApiProperty } from '@nestjs/swagger';

export class PlaylistDto {
    @ApiProperty()
    readonly userId: string;

    @ApiProperty()
    readonly collectionId: string;

    @ApiProperty()
    readonly bookId: string;
}