import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { CollectionModule } from './collection/collection.module';
import { BookModule } from './book/book.module';
import { CategoryModule } from './category/category.module';
import { PlaylistModule } from './playlist/playlist.module';
import { WatchlistModule } from './watchlist/watchlist.module';
import { GenreModule } from "./genre/genre.module";
import {ChapterModule} from "./chapter/chapter.module";

@Module({
  imports: [
      TypeOrmModule.forRoot({autoLoadEntities: true}),
      BookModule,
      CollectionModule,
      CategoryModule,
      PlaylistModule,
      WatchlistModule,
      GenreModule,
      ChapterModule
  ],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule {
  constructor(private connection: Connection) {}
}
