import { ApiProperty } from '@nestjs/swagger';

export class GenreDto {
    @ApiProperty()
    readonly name: string;
}