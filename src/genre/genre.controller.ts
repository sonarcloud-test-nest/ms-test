import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GenreService } from "./genre.service";
import { Genre } from "../entity/genre.entity";
import { GenreDto } from "./dto/genre.dto";

@ApiTags('Genre')
@Controller('genre')
export class GenreController {

    constructor(private genreService: GenreService) {}

    @Get()
    async all(): Promise<Genre[]> {
        return await this.genreService.all();
    }

    @Get(':id')
    async find(@Param('id') id: string): Promise<Genre> {
        return await this.genreService.find(id);
    }

    @Post('add')
    async create(@Body() category: GenreDto) {
        return await this.genreService.create(category);
    }

    @Put('edit/:id')
    async update(@Param('id') id:string, @Body() category: GenreDto) : Promise<Genre> {
        return await this.genreService.update(id, category);
    }

    @Delete('delete/:id')
    async delete(@Param('id') id:string): Promise<void> {
        return await this.genreService.remove(id);
    }
}