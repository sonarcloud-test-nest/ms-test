import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Entity } from 'typeorm';
import {Genre} from "../entity/genre.entity";
import {GenreDto} from "./dto/genre.dto";

@Injectable()
export class GenreService {
    constructor(
        @InjectRepository(Genre)
        private readonly genreRepository: Repository<Genre>
    ) {}

    all(): Promise<Genre[]> {
        return this.genreRepository.find();
    }

    find(id: string): Promise<Genre> {
        return this.genreRepository.findOneOrFail(id);
    }

    async create(genre: GenreDto): Promise<Genre> {
        return await this.genreRepository.save(genre);
    }

    async update(id: string, genre: GenreDto): Promise<Genre | null> {

        const cat = await this.genreRepository.findOneOrFail(id);

        if (!cat) {
            throw new NotFoundException(`This genre "${id}" doesn't exist.`)
        }

        await this.genreRepository.update(id, genre);
        return await this.genreRepository.findOne(id);
    }

    async remove(id: string): Promise<void> {
        let genre = await this.genreRepository.findOne(id);

        if (!genre) {
            throw new NotFoundException(`This genre "${id}" doesn't exist.`)
        }

        await this.genreRepository.remove(genre);
    }
}