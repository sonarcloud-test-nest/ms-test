import { getRepository, getConnection, Equal } from 'typeorm';
import { Watchlist } from '../../entity/watchlist.entity';

const faker = require('faker/locale/fr');

export async function watchlistSeeder() {
    console.log('Seeding Watchlist ...');

    // Init the array of playlist
    const watchlists = [];

    /*// Init the repositories
    const userRepository = await getRepository(User);
    const pageRepository = await getRepository(Page);

    // Init the variables
    const users = await userRepository.find();
    const pages = await pageRepository.find();

    users.forEach(user => {
        let randomNumbers = [];
        for (var i = 0; i <= 5; i++) {
            let watchlist = new Watchlist();
            watchlist.user = user;

            let random = Math.floor(Math.random() * Math.floor(pages.length));
            while (randomNumbers.includes(random)) {
                random = Math.floor(Math.random() * Math.floor(pages.length));
            }
            randomNumbers.push(random);

            watchlist.lastPage = pages[random];
            watchlist.bookId = pages[random].bookId;

            watchlists.push(watchlist);
        }
    });*/

    await getConnection().manager.save(watchlists).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}