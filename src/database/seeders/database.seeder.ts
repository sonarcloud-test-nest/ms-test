import { createConnection, getConnection } from 'typeorm';
import { collectionSeeder } from './collection.seeder';
import { bookSeeder } from './book.seeder';
import { categorySeeder } from './category.seeder';
import { genreSeeder } from './genre.seeder';
import { playlistSeeder } from './playlist.seeder';
import {watchlistSeeder} from "./watchlist.seeder";
import {chapterSeeder} from "./chapter.seeder";

module.exports = {
    run: async function() {
        //Init the connection
        await createConnection();

        //Seeders
        await categorySeeder();
        await genreSeeder();
        await collectionSeeder();
        await bookSeeder();
        await chapterSeeder();
        await playlistSeeder();
        await watchlistSeeder();

        console.log('All seeders executed successfully');
        process.exit();
    },
    clear: async function() {
        await createConnection();
        await getConnection().synchronize(true)
            .then(() => {
                console.log('Database cleared successfully');
                process.exit();
            })
            .catch(error => {
                console.log(error, 'fail');
                process.exit(1);
            });
    }
}
require('make-runnable');