import { getConnection } from "typeorm";
import { Genre } from "../../entity/genre.entity";

export async function genreSeeder() {
    console.log("Seeding genres ...");

    const genres = [];
    const names  = ['Anticipation', 'Dystopie', 'Post-apocalypse', 'Fantasy', 'Horror', 'Mystery', 'Romance',
        'Science-Fiction', 'Thriller', 'Suspense', 'Western', 'Superhero', 'Contemporain', 'Comedie', 'Isekai',
        'Historique', 'Psychologique', 'Sport', 'Paranormal', 'Enquete', 'Combat', 'Guerre', 'TimeTravel',
        'Extraterrestre', 'Réincarnation', 'Science', 'Aventure', 'Utopie', 'Action', 'Conspiration', 'Desastre',
        'Medicale', 'Politique', 'Religion'];

    names.forEach(name => {
        let genre = new Genre();
        genre.name = name;

        genres.push(genre);
    });

    await getConnection().manager.save(genres).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}