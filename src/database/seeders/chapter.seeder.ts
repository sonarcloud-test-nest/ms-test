import { getConnection, getRepository } from "typeorm";
import {Chapter} from "../../entity/chapter.entity";
import {Book} from "../../entity/book.entity";

const faker = require('faker/locale/fr');

export async function chapterSeeder() {
    console.log("Seeding chapters ...");

    const chapters = [];
    const bookRepository = await getRepository(Book);
    const books = await bookRepository.find();

    books.forEach(book => {
        for(let i = 0; i <= 5; i++) {
            let chapter = new Chapter();
            chapter.name = faker.random.words();
            chapter.index = (i+1);
            chapter.bookId = book.id;

            chapters.push(chapter);
        }
    });

    await getConnection().manager.save(chapters).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}