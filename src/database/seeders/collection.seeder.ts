import { getConnection, getRepository } from "typeorm";
import { Collection } from "../../entity/collection.entity";
import {Category} from "../../entity/category.entity";

const faker = require('faker/locale/fr');

export async function collectionSeeder() {
    console.log("Seeding collections ...");

    let collections = [];

    const categoryRepository = await getRepository(Category);
    const categories: Category[] = await categoryRepository.find();

    for (var i = 0; i < 20; i++) {
        let collection = new Collection();

        let category: Category = categories[1];

        collection.name = faker.random.words();
        collection.description = faker.lorem.paragraph();
        collection.filepath = faker.image.image();
        collection.categories = [category];
        collection.chapters = Math.floor(Math.random() * Math.floor(8));

        collections.push(collection);
    }

    await getConnection().manager.save(collections).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}