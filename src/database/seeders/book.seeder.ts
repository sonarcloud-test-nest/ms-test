import { getRepository, getConnection } from "typeorm";
import { Book } from "../../entity/book.entity";
import { Collection } from "../../entity/collection.entity";
import {Category} from "../../entity/category.entity";
import {Genre} from "../../entity/genre.entity";

const faker = require('faker/locale/fr');

export async function bookSeeder() {
    console.log("Seeding books ...");

    const books = [];

    const collectionRepository = await getRepository(Collection);
    const categoryRepository = await getRepository(Category);
    const genreRepository = await getRepository(Genre);
    const collections = await collectionRepository.find();
    const categories: Category[] = await categoryRepository.find();
    const genres: Genre[] = await genreRepository.find();

    for (var i = 0; i < 200; i++) {
        let book = new Book();
        let category: Category = categories[Math.floor(Math.random() * categories.length)];
        //let genre: Genre[] = await setRandomGenre(genres);

        book.name = faker.random.words();
        book.author = faker.name.lastName();
        book.editor = faker.name.lastName();
        book.chapters = faker.random.number({'min': 1, 'max': 5});
        book.bookPrice = faker.random.number({'min': 12, 'max': 30});
        book.pagePrice = faker.random.number({'min': 0, 'max': 2, precision: 0.01});
        book.description = faker.lorem.paragraph();
        book.categories = [category];

        let ids = [];
        let genresPushed = [];
        for (let j = 0; j < 4; j++) {
            let random = Math.floor(Math.random() * genres.length);
            let genre = genres[random];

            while (ids.includes(genre.id)) {
                random = Math.floor(Math.random() * genres.length);
                genre = genres[random]
            }

            ids.push(genre.id);
            genresPushed.push(genre)
        }

        book.genres = genresPushed;
        book.collection = await collections[i];

        books.push(book);
    }

    await getConnection().manager.save(books).catch((err) => {
        console.log(err);
        process.exit(1);
    });

    function setRandomGenre(genres) {
        genres.sort(()=> Math.random() - 0.5)
        let max = Math.floor(Math.random() * (10)) + 1;
        let genresList = [];

        genres.forEach((genre) => {
            if(genresList.indexOf(genre) === -1){
                genresList.push(genre)
            }
        });

        return genresList;
    }
}