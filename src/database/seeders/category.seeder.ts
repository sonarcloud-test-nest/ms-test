import { getConnection } from "typeorm";
import { Category } from "../../entity/category.entity";

export async function categorySeeder() {
    console.log("Seeding categories ...");

    const categories = [];
    const names  = ['Shonen', 'Shojo', 'Seinen', 'Kodomo', 'Bande dessinée', 'Roman'];

    names.forEach(name => {
        let category = new Category();
        category.name = name;

        categories.push(category);
    });

    await getConnection().manager.save(categories).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}