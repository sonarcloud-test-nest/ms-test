import { getRepository, getConnection } from 'typeorm';
import { Collection } from '../../entity/collection.entity';
import {Book} from "../../entity/book.entity";
import {Playlist} from "../../entity/playlist.entity";

const faker = require('faker/locale/fr');

export async function playlistSeeder() {
    console.log('Seeding playlist ...');

    // Init the array of playlist
    const playlists = [];

    // Init the repositories
    const collectionRepository = await getRepository(Collection);
    const bookRepository = await getRepository(Book);

    // Init the variables
    const collections = await collectionRepository.find();
    const books = await bookRepository.find();
    const userIds = ['68d5e2aa-e787-447b-8700-ae12f9e619fc'];

    userIds.forEach(userId => {
        let randomNumbers = [];
        for (var i = 0; i <= 5; i++) {
            let playlist = new Playlist();
            playlist.userId = userId;

            let random = Math.floor(Math.random() * Math.floor(collections.length));

            while (randomNumbers.includes(random)) {
                random = Math.floor(Math.random() * Math.floor(collections.length));
            }
            randomNumbers.push(random);

            if (i % 2 === 1) {
                playlist.collection = collections[random];
            } else {
                playlist.book = books[random];
            }
            playlists.push(playlist);
        }
    });

    await getConnection().manager.save(playlists).catch((err) => {
        console.log(err);
        process.exit(1);
    });
}