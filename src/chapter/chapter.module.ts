import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Chapter} from "../entity/chapter.entity";
import {ChapterService} from "./chapter.service";
import {ChapterController} from "./chapter.controller";

@Module({
    imports: [TypeOrmModule.forFeature([Chapter])],
    providers: [ChapterService],
    controllers: [ChapterController],
})
export class ChapterModule {}