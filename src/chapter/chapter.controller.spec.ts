import {Test, TestingModule} from '@nestjs/testing';
import {ChapterController} from "./chapter.controller";
import {ChapterService} from "./chapter.service";

describe("ChapterController", () => {
    let controller: ChapterController;

    const mockChapterService = {
        create: jest.fn(dto => {
            return {
                id: "uuidgenerate",
                ...dto
            }
        })
    }

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ChapterController],
            providers: [ChapterService]
        })
            .overrideProvider(ChapterService)
            .useValue(mockChapterService)
            .compile();

        controller = module.get<ChapterController>(ChapterController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    it('should create a chapter', () => {
        expect(controller.create({name: "MonChapitre", index: 1, bookId: "uuid"})).toEqual({
           id: expect.any(String),
           name: "MonChapitre",
           index: 1,
           bookId: "uuid"
        });
    })
});
