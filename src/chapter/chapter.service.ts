import { Injectable, NotFoundException, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Chapter } from "../entity/chapter.entity";
import { ChapterDto } from "./dto/chapter.dto";
import {Book} from "../entity/book.entity";

@Injectable()
export class ChapterService {
    constructor(@InjectRepository(Chapter) private readonly chapterRepository: Repository<Chapter>) {}

    find(id: string): Promise<Chapter> {
        return this.chapterRepository.findOneOrFail(id);
    }

    findByBook(bookId: string): Promise<Chapter[]> {
        return this.chapterRepository.find({
            where: {
                bookId: bookId
            },
            order: {
                index: "ASC"
            }
        });
    }

    async create(chapterDto: ChapterDto): Promise<Chapter> {
        return await this.chapterRepository.save(chapterDto);
    }

    async update(id: string, chapter: ChapterDto): Promise<Chapter | null> {
        const data: Chapter = await this.chapterRepository.findOneOrFail(id);

        if (!data) {
            throw new NotFoundException(`This chapter "${id}" doesn't exist.`)
        }

        await this.chapterRepository.update(id, chapter);
        return await this.chapterRepository.findOne(id);
    }

    async remove(id: string): Promise<void> {
        let chapter = await this.chapterRepository.findOne(id);

        if (!chapter) {
            throw new NotFoundException(`This watchlist "${id}" doesn't exist.`)
        }

        await this.chapterRepository.remove(chapter);
    }

    async removeByBookId(bookId: string): Promise<void> {
        let chapters = await this.chapterRepository.find({
            where: {
                bookId: bookId
            },
            order: {
                index: "ASC"
            }
        });

        if (chapters.length === 0) {
            throw new NotFoundException(`This bookId "${bookId}" doesn't exist.`)
        }

        await this.chapterRepository.remove(chapters);
    }
}