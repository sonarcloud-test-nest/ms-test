import { ApiProperty } from '@nestjs/swagger';

export class ChapterDto {
    @ApiProperty()
    readonly name: string;

    @ApiProperty()
    readonly index: number;

    @ApiProperty()
    readonly bookId: string;
}