import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import {ChapterService} from "./chapter.service";
import {Chapter} from "../entity/chapter.entity";
import {ChapterDto} from "./dto/chapter.dto";
import {Book} from "../entity/book.entity";

@ApiTags('Chapter')
@Controller('chapter')
export class ChapterController {

    constructor(private chapterService: ChapterService) {}

    @Get(':id')
    async find(@Param('id') id: string): Promise<Chapter> {
        return await this.chapterService.find(id);
    }

    @Get('book/:bookId')
    async findByBookId(@Param('bookId') bookId: string): Promise<Chapter[]> {
        return await this.chapterService.findByBook(bookId);
    }

    @Post('add')
    create(@Body() chapter: ChapterDto) {
        return this.chapterService.create(chapter);
    }

    @Put('edit/:id')
    update(@Param('id') id:string, @Body() chapter: ChapterDto) : Promise<Chapter> {
        return this.chapterService.update(id, chapter);
    }

    @Delete('delete/:id')
    delete(@Param('id') id:string): Promise<void> {
        return this.chapterService.remove(id);
    }

    @Delete('delete/book/:bookId')
    deleteByBook(@Param('bookId') bookId:string): Promise<void> {
        return this.chapterService.removeByBookId(bookId);
    }
}