import { Entity, Column, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, ManyToMany, JoinTable } from 'typeorm';
import { Book } from "./book.entity";
import { Category } from "./category.entity";
import {Playlist} from "./playlist.entity";
import {Genre} from "./genre.entity";

@Entity({name: 'collection'})
export class Collection {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column("text")
    description: string;

    @Column({ nullable: true})
    filepath: string;

    @Column()
    chapters: number;

    @Column({ default: true })
    isActive: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

    @OneToMany(() => Book, book => book.collection)
    books: Book[];

    @ManyToMany(() => Category)
    @JoinTable({ name: "collection_categories"})
    categories: Category[];

    @ManyToMany(() => Genre)
    @JoinTable({ name: "collection_genres"})
    genres: Genre[];

    @OneToMany(type => Playlist, playlist => playlist.collection)
    playlist: Playlist[];
}