import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, OneToOne, JoinColumn } from 'typeorm';

@Entity({name: 'genre'})
export class Genre {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @OneToOne(() => Genre)
    @JoinColumn()
    parent: Genre;

    @CreateDateColumn()
    createdAt: Date;
}