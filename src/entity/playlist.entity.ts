import { Entity, Column, CreateDateColumn, ManyToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';
import {Collection} from "./collection.entity";
import {Book} from "./book.entity";

@Entity('playlist')
export class Playlist {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    userId: string;

    @Column({ nullable: true })
    collectionId: string;

    @ManyToOne(() => Collection, collection => collection.playlist)
    @JoinColumn({ name: "collectionId"})
    collection: Collection

    @Column({ nullable: true })
    bookId: string;

    @ManyToOne(() => Book, book => book.playlist)
    @JoinColumn({ name: "bookId"})
    book: Book

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;
}