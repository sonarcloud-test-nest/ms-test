import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, OneToOne, JoinColumn } from 'typeorm';

@Entity({name: 'category'})
export class Category {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @OneToOne(() => Category)
    @JoinColumn()
    parent: Category;

    @CreateDateColumn()
    createdAt: Date;
}