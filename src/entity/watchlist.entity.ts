import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { Book } from './book.entity';

@Entity({name: 'watchlist'})
export class Watchlist {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    userId: string;

    @Column()
    bookId: string;

    @ManyToOne(() => Book, book => book.watchlist)
    @JoinColumn({ name: 'bookId'})
    book: Book;

    @Column()
    lastPageId: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}