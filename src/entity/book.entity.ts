import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToMany,
    JoinTable,
    ManyToOne
} from 'typeorm';
import { Collection } from './collection.entity';
import { Playlist } from './playlist.entity';
import { Watchlist } from './watchlist.entity';
import {Category} from "./category.entity";
import {Genre} from "./genre.entity";
import {Chapter} from "./chapter.entity";

@Entity({name: 'book'})
export class Book {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column()
    author: string;

    @Column()
    editor: string;

    @Column('text')
    description: string;

    @Column()
    chapters: number;

    @Column("double")
    bookPrice: number;

    @Column("double")
    pagePrice: number;

    @Column({ default: true })
    isActive: boolean;

    @ManyToMany(() => Category)
    @JoinTable({ name: "book_categories"})
    categories: Category[];

    @ManyToMany(() => Genre)
    @JoinTable({ name: "book_genres"})
    genres: Genre[];

    @Column({ nullable: true })
    collectionId: string;

    @ManyToOne(() => Collection, collection => collection.books)
    @JoinColumn({ name: "collectionId"})
    collection: Collection;

    @OneToMany(() => Chapter, chapter => chapter.book)
    chapter: Chapter[];

    @OneToMany(() => Playlist, playlist => playlist.book)
    playlist: Playlist[];

    @OneToMany(() => Watchlist, watchlist => watchlist.book)
    watchlist: Watchlist[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

}