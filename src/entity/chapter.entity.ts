import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, OneToOne, JoinColumn, ManyToOne} from 'typeorm';
import { Book } from "./book.entity";

@Entity({name: 'chapter'})
export class Chapter {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    index: number;

    @Column({ nullable: true })
    bookId: string;

    @ManyToOne(() => Book, book => book.chapter)
    @JoinColumn({ name: "bookId"})
    book: Book

    @CreateDateColumn()
    createdAt: Date;
}