import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from "../entity/book.entity";
import { CreateBookDto } from "./dto/create_book.dto";
import { UpdateBookDto } from "./dto/update_book.dto";

@Injectable()
export class BookService {
    constructor(
        @InjectRepository(Book)
        private readonly bookRepository: Repository<Book>
    ) {}

    all(): Promise<Book[]> {
        return this.bookRepository.find({ relations: ['collection'] });
    }

    /**
     * @param categoryId
     * @param genreIds
     * @param skip
     * @param take
     */
    findByCategoryId(categoryId, genreIds, skip, take): Promise<Book[]> {
        const qb = this.bookRepository
            .createQueryBuilder("book")
            .leftJoinAndSelect("book.categories", "category")
            .leftJoinAndSelect("book.genres", "genre")
            .where("book_category.categoryId = :id", {
                id: categoryId
            });

        if (genreIds != null && Array.isArray(genreIds)) {
            qb.andWhere("book_genre.genreId IN (:...ids)", { ids: genreIds });
        }

        if (take !== null) qb.take(take);
        if (skip !== null && take !== null) qb.skip(skip);

        return qb.orderBy('book.id', 'ASC').getMany();
    }

    find(id: string): Promise<Book> {
        return this.bookRepository.findOneOrFail(id, { relations: ['collection', 'categories', 'genres'] });
    }

    async create(book: CreateBookDto): Promise<Book> {
        return await this.bookRepository.save(book);
    }

    async update(id: string, book: UpdateBookDto): Promise<Book | null> {

        const data = await this.bookRepository.findOneOrFail(id);

        if (!data) {
            throw new NotFoundException(`This book "${id}" doesn't exist.`)
        }

        if (data.id) {
            await this.bookRepository.update(id, book);
            return await this.bookRepository.findOne(id);
        }
    }

    async remove(id: string): Promise<void> {
        let book = await this.bookRepository.findOne(id);

        if (!book) {
            throw new NotFoundException(`This book "${id}" doesn't exist.`)
        }

        await this.bookRepository.softRemove(book);
    }
}