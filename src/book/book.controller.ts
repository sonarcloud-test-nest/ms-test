import { Controller, Get, Post, Put, Delete, Param, Body, Req } from '@nestjs/common';
import { Request } from 'express';
import { ApiTags } from '@nestjs/swagger';
import { BookService } from "./book.service";
import { Book } from "../entity/book.entity";
import { CreateBookDto } from "./dto/create_book.dto";
import { UpdateBookDto } from "./dto/update_book.dto";

@ApiTags('Book')
@Controller('book')
export class BookController {

    constructor(private bookService: BookService) {}

    @Get()
    async all(): Promise<Book[]> {
        return await this.bookService.all();
    }

    @Get(':id')
    find(@Param('id') id: string): Promise<Book> {
        return this.bookService.find(id);
    }

    @Get('/category/:categoryId')
    async findByCategoryId(@Param('categoryId') categoryId: string, @Req() request: Request): Promise<Book[]> {
        let genres = (request.query['genres'] !== undefined) ? request.query['genres'] : null;
        let skip = (request.query['skip'] !== undefined) ? request.query['skip'] : null;
        let take = (request.query['take'] !== undefined) ? request.query['take'] : null;

        if (typeof genres === "string") {
            genres = genres.replace("[", "");
            genres = genres.replace("]", "");
            genres = genres.split(",");
        }

        return await this.bookService.findByCategoryId(categoryId, genres, skip, take);
    }

    @Post('add')
    create(@Body() book: CreateBookDto) {
        return this.bookService.create(book);
    }

    @Put('edit/:id')
    update(@Param('id') id:string, @Body() book: UpdateBookDto) : Promise<Book> {
        return this.bookService.update(id, book);
    }

    @Delete('delete/:id')
    delete(@Param('id') id:string): Promise<void> {
        return this.bookService.remove(id);
    }
}