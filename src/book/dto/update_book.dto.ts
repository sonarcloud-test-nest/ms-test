import { ApiProperty } from '@nestjs/swagger';

export class UpdateBookDto {
    @ApiProperty()
    readonly name: string;

    @ApiProperty()
    readonly author: string;

    @ApiProperty()
    readonly editor: string;

    @ApiProperty()
    readonly bookPrice: number;

    @ApiProperty()
    readonly pagePrice: number;

    @ApiProperty()
    readonly description: string;

    @ApiProperty()
    readonly chapters: number;

    @ApiProperty()
    readonly isActive: boolean;

    @ApiProperty()
    readonly idCollection: string;

    @ApiProperty()
    readonly updatedAt: Date;
}