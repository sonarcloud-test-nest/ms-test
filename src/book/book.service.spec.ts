import {Test, TestingModule} from '@nestjs/testing';
import {BookService} from "./book.service";
import {getRepositoryToken} from "@nestjs/typeorm";
import {Book} from "../entity/book.entity";
import {v4 as uuidv4} from 'uuid';
import {Collection} from "../entity/collection.entity";
import {Category} from "../entity/category.entity";
import {Genre} from "../entity/genre.entity";

describe("BookService", () => {
    let service: BookService;
    let fakeBookDto = {
        name: "Mon super livre",
        author: "Vervisch Florian",
        editor: "Aston",
        bookPrice: 7.5,
        pagePrice: 0.54,
        description: "Ma description",
        chapters: 5,
        isActive: true,
        idCollection: ""
    }

    const mockBookRepository = {
        save: jest.fn().mockImplementation(book => Promise.resolve({
            id: expect.any(String),
            ...book
        })),
        findOneOrFail: jest.fn().mockImplementation(book => book),
        softRemove: jest.fn(),
        findOne: jest.fn().mockImplementation(book => book)
    }

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [BookService, {
                provide: getRepositoryToken(Book),
                useValue: mockBookRepository
            }]
        }).compile();

        service = module.get<BookService>(BookService);
    });

    it('should be defined', function () {
        expect(service).toBeDefined();
    });

    it('should create a new book and return it', async () => {
        expect(await service.create(fakeBookDto)).toEqual({
            id: expect.any(String),
            ...fakeBookDto
        });
    });

    it('should return a book', function () {
        let book = {
            id: uuidv4(),
            ...fakeBookDto,
            collection: new Collection(),
            categories: [new Category()],
            genres: [new Genre()]
        }

        mockBookRepository.findOneOrFail.mockReturnValue(book);
        expect(service.find(book.id)).toEqual(book);
        expect(mockBookRepository.findOneOrFail).toHaveBeenCalledWith(book.id, {
            relations: ["collection", "categories", "genres"]
        });
    });

    it('should remove a book', async function () {
       let id: string = uuidv4();
       await service.remove(id);
       expect(await mockBookRepository.softRemove).toHaveBeenCalledWith(id);
    });
});
