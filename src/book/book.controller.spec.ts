import {Test, TestingModule} from '@nestjs/testing';
import {BookService} from "./book.service";
import {BookController} from "./book.controller";
import {v4 as uuidv4} from 'uuid';
import {Collection} from "../entity/collection.entity";
import {Category} from "../entity/category.entity";
import {Genre} from "../entity/genre.entity";

describe("BookController", () => {
   let controller: BookController;
   let fakeBookDto = {
        name: "Mon super livre",
        author: "Vervisch Florian",
        editor: "Aston",
        bookPrice: 7.5,
        pagePrice: 0.54,
        description: "Ma description",
        chapters: 5,
        isActive: true,
        idCollection: ""
    }

   const mockBookService = {
       create: jest.fn((dto) => {
           return {
               id: uuidv4(),
               ...dto
           }
       }),
       update: jest.fn((id, dto) => {
           return {
               id: id,
               ...dto
           }
       }),
       find: jest.fn().mockImplementation((id) => {
           return Promise.resolve({
               id: id,
               ...fakeBookDto
           })
       }),
       remove: jest.fn()
   }

   beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
          controllers: [BookController],
          providers: [BookService]
      })
          .overrideProvider(BookService)
          .useValue(mockBookService)
          .compile();

      controller = module.get<BookController>(BookController);
   });

    it('should be defined', function () {
        expect(controller).toBeDefined();
    });

    it('should create a book', function () {
        expect(controller.create(fakeBookDto)).toEqual({
            id: expect.any(String),
            ...fakeBookDto
        });

        expect(mockBookService.create).toHaveBeenCalledWith(fakeBookDto);
    });

    it('should update a user', function () {
        let id = uuidv4();
        let fakeUpdateBook = { ...fakeBookDto, updatedAt: new Date() };

        expect(controller.update(id, fakeUpdateBook)).toEqual({
            id: id,
            ...fakeUpdateBook
        });

        expect(mockBookService.update).toHaveBeenCalled();
    });

    it('should find a book', function () {
       let book = {
           id: uuidv4(),
           ...fakeBookDto,
           collection: new Collection(),
           categories: [new Category()],
           genres: [new Genre()],
       }

       mockBookService.find.mockReturnValue(book);
       expect(controller.find(book.id)).toEqual(book);
       expect(mockBookService.find).toHaveBeenCalledWith(book.id);
    });

    it('sould remove a book', function () {
       let id: string = uuidv4();

       controller.delete(id);
       expect(mockBookService.remove).toHaveBeenCalledWith(id);
    });
});
