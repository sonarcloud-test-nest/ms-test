import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Entity } from 'typeorm';
import {Collection} from "../entity/collection.entity";
import { CreateCollectionDto } from "./dto/create_collection.dto";
import {UpdateCollectionDto} from "./dto/update_collection.dto";

@Injectable()
export class CollectionService {
    constructor(
        @InjectRepository(Collection)
        private readonly collectionRepository: Repository<Collection>
    ) {}

    all(): Promise<Collection[]> {
        return this.collectionRepository.find();
    }

    find(id: string): Promise<Collection> {
        return this.collectionRepository.findOneOrFail(id, { relations: ['books', 'categories', 'genres'] });
    }

    findByCategoryId(categoryId, skip, take): Promise<Collection[]> {
        const qb = this.collectionRepository
            .createQueryBuilder("book")
            .leftJoinAndSelect("book.categories", "category")
            .where("category.id = :id", {
                id: categoryId
            });

        if (take !== null) qb.take(take);
        if (skip !== null && take !== null) qb.skip(skip);

        return qb.getMany();
    }

    async create(collection: CreateCollectionDto): Promise<Collection> {
        return await this.collectionRepository.save(collection);
    }

    async update(id: string, collection: UpdateCollectionDto): Promise<Collection | null> {

        const coll = await this.collectionRepository.findOneOrFail(id);

        if (!coll) {
            throw new NotFoundException(`This collection "${id}" doesn't exist.`)
        }

        await this.collectionRepository.update(id, collection);
        return await this.collectionRepository.findOne(id);
    }

    async remove(id: string): Promise<void> {
        let collection = await this.collectionRepository.findOne(id);

        if (!collection) {
            throw new NotFoundException(`This collection "${id}" doesn't exist.`)
        }

        await this.collectionRepository.softRemove(collection);
    }
}