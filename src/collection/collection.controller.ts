import {Controller, Get, Post, Put, Delete, Param, Body, Req} from '@nestjs/common';
import {CollectionService} from "./collection.service";
import { CreateCollectionDto } from "./dto/create_collection.dto";
import {Collection} from "../entity/collection.entity";
import {UpdateCollectionDto} from "./dto/update_collection.dto";
import { ApiTags } from '@nestjs/swagger';
import {Request} from "express";

@ApiTags('Collection')
@Controller('collection')
export class CollectionController {

    constructor(private collectionService: CollectionService) {}

    @Get()
    async all(): Promise<Collection[]> {
        return await this.collectionService.all();
    }

    @Get(':id')
    async find(@Param('id') id: string): Promise<Collection> {
        return await this.collectionService.find(id);
    }

    @Get('/category/:categoryId')
    async findByCategoryId(@Param('categoryId') categoryId: string, @Req() request: Request): Promise<Collection[]> {
        let skip = (request.query['skip'] !== undefined) ? request.query['skip'] : null;
        let take = (request.query['take'] !== undefined) ? request.query['take'] : null;

        return await this.collectionService.findByCategoryId(categoryId, skip, take);
    }

    @Post('add')
    async create(@Body() collection: CreateCollectionDto) {
        return await this.collectionService.create(collection);
    }

    @Put('edit/:id')
    async update(@Param('id') id:string, @Body() collection: UpdateCollectionDto) : Promise<Collection> {
        return await this.collectionService.update(id, collection);
    }

    @Delete('delete/:id')
    async delete(@Param('id') id:string): Promise<void> {
        return await this.collectionService.remove(id);
    }
}