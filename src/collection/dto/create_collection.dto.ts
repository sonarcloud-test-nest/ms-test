import { ApiProperty } from '@nestjs/swagger';

export class CreateCollectionDto {
    @ApiProperty()
    readonly name: string;

    @ApiProperty()
    readonly description: string;

    @ApiProperty()
    readonly filepath: string;

    @ApiProperty()
    readonly chapters: number;
}