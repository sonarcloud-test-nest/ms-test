import { ApiProperty } from '@nestjs/swagger';

export class UpdateCollectionDto {
    @ApiProperty()
    readonly name: string;

    @ApiProperty()
    readonly description: string;

    @ApiProperty()
    readonly filepath: string;

    @ApiProperty()
    readonly chapters: number;

    @ApiProperty()
    readonly isActive: boolean;

    @ApiProperty()
    readonly updatedAt: Date;
}