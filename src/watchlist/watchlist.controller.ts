import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Watchlist } from '../entity/watchlist.entity';
import { WatchlistDto } from './dto/watchlist.dto';
import { WatchlistService } from './watchlist.service';

@ApiTags('Watchlist')
@Controller('watchlist')
export class WatchlistController {
    constructor(private watchlistService: WatchlistService) {}

    @Get(':id')
    async find(@Param('id') id: string): Promise<Watchlist> {
        return await this.watchlistService.find(id);
    }

    @Get('user/:userId')
    async findByUser(@Param('userId') userId: string): Promise<Watchlist[]> {
        return await this.watchlistService.findByUser(userId);
    }

    @Get('user/:userId/book/:bookId')
    async findByUserAndBook(@Param('userId') userId: string, @Param('bookId') bookId: string): Promise<Watchlist> {
        return await this.watchlistService.findByUserAndBook(userId, bookId);
    }

    @Put('edit/:id')
    async update(@Param('id') id: string, @Body() watchlist: WatchlistDto) : Promise<Watchlist> {
        return await this.watchlistService.update(id, watchlist);
    }

    @Post('add')
    async create(@Body() watchlist: WatchlistDto) {
        return await this.watchlistService.create(watchlist);
    }

    @Delete('delete/:id')
    async delete(@Param('id') id:string): Promise<void> {
        return await this.watchlistService.remove(id);
    }
}