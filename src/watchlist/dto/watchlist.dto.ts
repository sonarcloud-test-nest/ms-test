import { ApiProperty } from '@nestjs/swagger';

export class WatchlistDto {
    @ApiProperty()
    readonly userId: string;

    @ApiProperty()
    readonly bookId: string;

    @ApiProperty()
    readonly lastPageId: string;
}