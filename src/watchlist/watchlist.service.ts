import { Injectable, NotFoundException, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Watchlist } from '../entity/watchlist.entity';
import { WatchlistDto } from './dto/watchlist.dto';

@Injectable()
export class WatchlistService {
    constructor(@InjectRepository(Watchlist) private readonly watchlistRepository: Repository<Watchlist>) {}

    find(id: string): Promise<Watchlist> {
        return this.watchlistRepository.findOneOrFail(id);
    }

    findByUser(userId: string): Promise<Watchlist[]> {
        return this.watchlistRepository.find({
            relations: ['book', 'book.collection'],
            where: {
                userId: userId
            },
            order: {
                createdAt: "DESC"
            }
        });
    }

    findByUserAndBook(userId: string, bookId: string): Promise<Watchlist> {
        return this.watchlistRepository.findOne({
            where: {
                userId: userId,
                bookId: bookId
            }
        });
    }

    async create(watchlistDto: WatchlistDto): Promise<Watchlist> {
        let watchlist = await this.watchlistRepository.find({
            where: {
                userId: watchlistDto.userId,
                bookId: watchlistDto.bookId
            }
        });

        if (watchlist.length == 0) {
            return await this.watchlistRepository.save(watchlistDto);
        } else {
            throw new HttpException('The watchlist already exist', 400);
        }
    }

    async update(id: string, watchlist: WatchlistDto): Promise<Watchlist | null> {
        const data: Watchlist = await this.watchlistRepository.findOneOrFail(id);

        if (!data || data.userId != watchlist.userId) {
            throw new NotFoundException(`This watchlist "${id}" doesn't exist.`)
        }

        if (data.id) {
            await this.watchlistRepository.update(id, watchlist);
            return await this.watchlistRepository.findOne(id);
        }
    }

    async remove(id: string): Promise<void> {
        let watchlist = await this.watchlistRepository.findOne(id);

        if (!watchlist) {
            throw new NotFoundException(`This watchlist "${id}" doesn't exist.`)
        }

        await this.watchlistRepository.remove(watchlist);
    }
}