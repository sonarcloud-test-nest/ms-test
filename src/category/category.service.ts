import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Entity } from 'typeorm';
import {Category} from "../entity/category.entity";
import {CategoryDto} from "./dto/category.dto";

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>
    ) {}

    all(): Promise<Category[]> {
        return this.categoryRepository.find();
    }

    find(id: string): Promise<Category> {
        return this.categoryRepository.findOneOrFail(id);
    }

    async create(category: CategoryDto): Promise<Category> {
        return await this.categoryRepository.save(category);
    }

    async update(id: string, category: CategoryDto): Promise<Category | null> {

        const cat = await this.categoryRepository.findOneOrFail(id);

        if (!cat) {
            throw new NotFoundException(`This category "${id}" doesn't exist.`)
        }

        await this.categoryRepository.update(id, category);
        return await this.categoryRepository.findOne(id);
    }

    async remove(id: string): Promise<void> {
        let category = await this.categoryRepository.findOne(id);

        if (!category) {
            throw new NotFoundException(`This category "${id}" doesn't exist.`)
        }

        await this.categoryRepository.remove(category);
    }
}