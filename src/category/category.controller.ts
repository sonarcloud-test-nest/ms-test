import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CategoryService } from "./category.service";
import { Category } from "../entity/category.entity";
import { CategoryDto } from "./dto/category.dto";

@ApiTags('Category')
@Controller('category')
export class CategoryController {

    constructor(private categoryService: CategoryService) {}

    @Get()
    async all(): Promise<Category[]> {
        return await this.categoryService.all();
    }

    @Get(':id')
    async find(@Param('id') id: string): Promise<Category> {
        return await this.categoryService.find(id);
    }

    @Post('add')
    async create(@Body() category: CategoryDto) {
        return await this.categoryService.create(category);
    }

    @Put('edit/:id')
    async update(@Param('id') id:string, @Body() category: CategoryDto) : Promise<Category> {
        return await this.categoryService.update(id, category);
    }

    @Delete('delete/:id')
    async delete(@Param('id') id:string): Promise<void> {
        return await this.categoryService.remove(id);
    }
}